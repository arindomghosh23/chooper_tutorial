import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chopper_app/data/post_api_service.dart';
import 'package:flutter_chopper_app/models/built_post.dart';
import 'package:flutter_chopper_app/ui/screen/single_post_page.dart';
import 'package:provider/provider.dart';
import 'package:built_collection/built_collection.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chopper blog'),
      ),
      body: _buildBody(context),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          final newPost = BuiltPost(
            (b) => b
              // id is null - it gets assigned in the backend
              ..title = 'New Title'
              ..body = 'New body',
          );
          final response =
              await Provider.of<PostApiService>(context, listen: false)
                  .postPost(newPost);
          print(response.body);
        },
      ),
    );
  }

  FutureBuilder<Response> _buildBody(BuildContext context) {
    return FutureBuilder<Response<BuiltList<BuiltPost>>>(
      future: Provider.of<PostApiService>(context).getPosts(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                snapshot.error.toString(),
                textAlign: TextAlign.center,
                textScaleFactor: 1.3,
              ),
            );
          }
          return _buildPost(context, snapshot.data.body);
        } else
          return Center(
            child: CircularProgressIndicator(),
          );
      },
    );
  }

  ListView _buildPost(BuildContext context, BuiltList<BuiltPost> posts) {
    return ListView.builder(
        itemCount: posts.length,
        padding: EdgeInsets.all(8),
        itemBuilder: (context, index) {
          return Card(
            elevation: 5,
            child: ListTile(
              title: Text(
                posts[index].title,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(posts[index].body),
              onTap: () {
                _navigateToPost(context, posts[index].id);
              },
            ),
          );
        });
  }

  void _navigateToPost(BuildContext context, int id) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SinglePostPage(
                  postId: id,
                )));
  }
}
