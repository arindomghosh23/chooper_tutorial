import 'package:chopper/chopper.dart';
import 'package:flutter_chopper_app/data/mobile_data_interceptor.dart';
import 'package:flutter_chopper_app/models/built_post.dart';
import 'package:built_collection/built_collection.dart';

import 'built_value_converter.dart';
part 'post_api_service.chopper.dart';

@ChopperApi(baseUrl: '/')
abstract class PostApiService extends ChopperService {
  @Get(path: 'posts')
  Future<Response<BuiltList<BuiltPost>>> getPosts();

  @Get(path: 'posts/{id}')
  Future<Response<BuiltPost>> getPost(@Path('id') int id);

  @Post(path: 'posts')
  Future<Response<BuiltPost>> postPost(@Body() BuiltPost body);

  static PostApiService create() {
    final chopperClient = ChopperClient(
        baseUrl: 'https://jsonplaceholder.typicode.com',
        services: [_$PostApiService()],
        converter:BuiltValueConverter(),// JsonConverter(),
        interceptors: [
//        HeadersInterceptor({'Cache-Control': 'no-cache'}),
//        HttpLoggingInterceptor(),
//        CurlInterceptor()

//          (Request request) async {
//            if (request.method == HttpMethod.Post) {
//              chopperLogger.info('Performed a POST request');
//            }
//            return request;
//          },
//          (Response response) async {
//            if (response.statusCode == 404) {
//              chopperLogger.severe('404 NOT FOUND');
//            }
//            return response;
//          },
          MobileDataInterceptor()
        ]);
    return _$PostApiService(chopperClient);
  }
}
