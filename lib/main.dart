import 'package:flutter/material.dart';
import 'package:flutter_chopper_app/ui/screen/home_page.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import 'data/post_api_service.dart';

void main() {
//  assert(() {
//    _setupLogging();
//  }());
  runApp(MyApp());
}

void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    print('${rec.level.name}: ${rec.time}: ${rec.message}');
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (_) => PostApiService.create(),
          dispose: (context, PostApiService service) =>
              service.client.dispose(),
        )
      ],
      child: MaterialApp(
        title: 'Chopper Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage(),
      ),
    );
  }
}
